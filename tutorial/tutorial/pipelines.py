# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import sqlite3
from os import path

from scrapy import signals
from scrapy import log

class SqlitePipeline(object):
    db_file = 'circular.db'

    def __init__(self):
        self.conn = None

    def open_spider(self, spider):
        if path.exists(self.db_file):
            self.conn = sqlite3.connect(self.db_file)
        else:
            self.conn = self.create_table(self.db_file)

        self.cursor = self.conn.cursor()

    def close_spider(self, spider):
        self.conn.commit()
        self.conn.close()

    def create_table(self, filename):
        conn = sqlite3.connect(self.db_file)
        conn.execute("""CREATE TABLE shoprite
                     (id INTEGER PRIMARY KEY AUTOINCREMENT   NOT NULL,
                     item_id TEXT,
                     title TEXT,
                     price REAL,
                     image TEXT,
                     brand TEXT)""")
        conn.commit()
        return conn

    def process_item(self, item, spider):
        try:
            # print('inside: ' + ''.join(item['title']))
            self.conn.execute("""INSERT INTO shoprite
                    (title, price, image, brand, item_id)
                    VALUES (:title, :price, :image, :brand, :item_id)""",
                        {
                        'title': item['title'],
                        'price': item['price'],
                        'image': item['image'],
                        'brand': item['brand'],
                        'item_id': item['item_id']
                        }
                    )
        except sqlite3.Error as e:
            log.msg('Failed to insert item: ' + ''.join(item['item_id']))
            log.msg('An error occurred:', e.args[0])
        return item
