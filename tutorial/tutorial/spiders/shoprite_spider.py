import scrapy
from tutorial.items import ShopriteItem

class ShopriteSpider(scrapy.Spider):

    """Docstring for ShopriteItems. """
    name = "shoprite"
    allowed_domains = ["plan.shoprite.com"]
    start_urls = [
            "http://plan.shoprite.com/Circular/ShopRite-of-Brookdale/F442733/Weekly/2/%s"% page for page in range(1,14)
            ]

    def parse(self, response):
        for sel in response.xpath("//div[starts-with(@id, 'CircularItem-')]"):
            item = ShopriteItem()
            item['image'] = ''.join(sel.xpath("div[@class='leftContent']/img/@src").extract())
            item['title'] = ''.join(sel.xpath('div[@class="rightContent"]/p[@class="itemTitle"]/text()').extract())
            item['price'] = ''.join(sel.xpath('div[@class="rightContent"]/p[@class="itemPrice"]/text()').extract())
            item['brand'] = ''.join(sel.xpath('div[@class="rightContent"]/p[@class="itemBrand"]/text()').extract())
            item['item_id'] = ''.join(sel.xpath('div[@class="rightContent"]/input/@data-id').extract())
            yield item
