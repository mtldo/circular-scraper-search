import scrapy
from tutorial.items import DmozItem

class DmozSpider(scrapy.Spider):

    """Docstring for DmozSpider. """
    name = "dmoz"
    allowed_domains = ["dmoz.org"]
    start_urls = [
            "http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
            "http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
    ]

    def parse(self, response):
        for sel in response.xpath("//div[contains(concat(' ', normalize-space(@class), ' '), ' title-and-desc ')]"):
            item = DmozItem()
            item['title'] = sel.xpath("a/div[@class='site-title']/text()").extract()
            item['link'] = sel.xpath('a/@href').extract()
            item['desc'] = sel.xpath("div[contains(@class,'site-descr')]/text()").extract()[0].strip()
            yield item

